[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fnicolalandro%2Ftorchbasics.git/HEAD)

# TorchBasics

Study torch, this is a repo in witch I put My resumed explanation of torch basics.

* Pytorch Basics: learn pytorch basics (autograd, models/layer, optimizers and loss function) by refactoring
* TorchDatasetAndDataloader: learn how to create Dataset, Sampler and treat data in general
* TorchAndCuda&FullExample: learn how to use Cuda, LR Scheduler and full example with training LeNet5 on Cifar10
